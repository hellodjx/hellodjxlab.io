---
title: Fcitx5 Chinese addons (C++)
summary: Write some tests for fcitx5 Chinese addons.
tags:
- Linux
date: "2020-08-25T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://github.com/fcitx/fcitx5-chinese-addons/commits?author=vifly

image:
  caption: https://github.com/fcitx
  focal_point: Smart
---
