---
title: Fcitx5 (C++)
summary: Add fcitx4 frontend to have better support on WPS and old implementation.
tags:
- Linux
date: "2021-01-10T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://github.com/fcitx/fcitx5/commits?author=vifly

image:
  caption: https://github.com/fcitx
  focal_point: Smart
---
