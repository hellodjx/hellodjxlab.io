---
title: Debian Translation
summary: Contribute some translations to Debian website. The number of lines updated are more than three thousand.
tags:
- Linux
date: "2020-01-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://salsa.debian.org/webmaster-team/webwml/-/commits/master?author=Vifly

image:
  caption: https://www.debian.org/logos/index
  focal_point: Smart
---
