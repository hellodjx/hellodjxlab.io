---
title: Debian Packaging
summary: Upload a software package to Debian Repository. Currently available for Debian unstable/testing users.
tags:
- Linux
date: "2020-05-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://packages.debian.org/unstable/prettyping

image:
  caption: https://www.debian.org/logos/index
  focal_point: Smart
---
