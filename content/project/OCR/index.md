---
title: OCR Platform (Backend)
summary: An online platform for Chinese OCR. Powered by FastAPI framework.
tags:
- Backend
date: "2020-12-25T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://gitlab.com/hellodjx/aiplatform

image:
  caption: https://gitlab.com/hellodjx/aiplatform
  focal_point: Smart
---
