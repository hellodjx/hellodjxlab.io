---
title: MapReduce
summary: A simple Golang implement for Google MapReduce.
tags:
- Distributed system
date: "2022-09-14T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://gitlab.com/hellodjx/mapreduce

image:
  caption: https://gitlab.com/hellodjx/mapreduce
  focal_point: Smart
---
