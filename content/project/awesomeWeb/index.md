---
title: awesomeWeb
summary: A simple Golang backend application with clear docs.
tags:
- Backend
date: "2022-09-14T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://gitlab.com/hellodjx/awesomeweb

image:
  caption: https://gitlab.com/hellodjx/awesomeweb
  focal_point: Smart
---
