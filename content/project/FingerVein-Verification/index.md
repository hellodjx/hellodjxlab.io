---
title: FingerVein Verification
summary: Implement many CNNs for finger vein verification.
tags:
- Deep Learning
date: "2020-04-30T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://gitlab.com/hellodjx/fingervein-verification

image:
  caption: https://gitlab.com/hellodjx/fingervein-verification
  focal_point: Smart
---
