---
title: "Finger Vein Verification Algorithm Based on Fully Convolutional Neural Network and Conditional Random Field"
authors:
- JunYing Zeng
- Fan Wang
- admin
- ...
date: "2020-03-28T00:00:00Z"
doi: "10.1109/ACCESS.2020.2984711"

# Schedule page publish date (NOT publication's date).
publishDate: "2020-04-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "IEEE Access"
publication_short: ""

abstract: Owing to the complexity of finger vein patterns in shape and spatial dependence, the existing methods suffer from an inability to obtain accurate and stable finger vein features. This paper, so as to compensate this defect, proposes an end-to-end model to extract vein textures through integrating the fully convolutional neural network (FCN) with conditional random field (CRF). Firstly, to reduce missing pixels during ROI extraction, the method of sliding window summation is employed to filter and adjusted with self-built tools. In addition, the traditional baselines are endowed with different weights to automatically assign labels. Secondly, the deformable convolution network, through replacing the plain counterparts in the standard U-Net mode, can capture the complex venous structural features by adaptively adjusting the receptive fields according to veins’ scales and shapes. Moreover, the above features can be further mined and accumulated by combining the recurrent neural network (RNN) and the residual network (ResNet). With the steps mentioned above, the fully convolutional neural network is constructed. Finally, the CRF with Gaussian pairwise potential conducts mean-field approximate inference as the RNN, and then is embedded as a part of the FCN, so that the model can fully integrate CRF with FCNs, which provides the possibility to involve the usual back-propagation algorithm in training the whole deep network end-to-end. The proposed models in this paper were tested on three public finger vein datasets SDUMLA, MMCBNU and HKPU with experimental results to certify their superior performance on finger-vein verification tasks compared with other equivalent models including U-Net.

# Summary. An optional shortened abstract.
summary: This paper proposes an end-to-end model to extract vein textures through integrating the fully convolutional neural network (FCN) with conditional random field (CRF).

tags:

featured: false

links:
# - name: ""
#   url: ""
url_pdf: https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9052656
# url_code: ''
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Example in paper'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: [FingerVein-Verification]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}
