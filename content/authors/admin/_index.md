---
# Display name
name: JianXiang Deng
avatar_image: ""
# Username (this should match the folder name)
authors:
- admin
# resume download button
# btn:
# - url : "https://sourcethemes.com/academic/docs/install/"
#   label : "Download Resume"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Student

# Organizations/Affiliations
organizations:
- name: Wuyi University
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include image segmentation of medical imaging and deep learning.

# Should the user's education and interests be displayed?
display_education: false

interests:
- Artificial Intelligence
- Computational Linguistics
- Information Retrieval

education:
  courses:
  - course: PhD in Artificial Intelligence
    institution: Stanford University
    year: 2012
  - course: MEng in Artificial Intelligence
    institution: Massachusetts Institute of Technology
    year: 2009
  - course: BSc in Artificial Intelligence
    institution: Massachusetts Institute of Technology
    year: 2008

# Social/academia Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:djianxiang@outlook.com'  # For a direct email link, use "mailto:test@example.org".

- icon: github
  icon_pack: fab
  link: https://github.com/vifly
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "djianxiang@outlook.com"
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

JianXiang Deng is a student of Wuyi University. His research interests include image segmentation of medical imaging and deep learning.
